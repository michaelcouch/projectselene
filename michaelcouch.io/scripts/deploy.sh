#!/bin/bash

branch=$1
if [[ "$branch" = "master" ]]; then
    rsync -vr --progress dist/* michaelcouch@michaelcouch.io:/home/michaelcouch/michaelcouch.io/.
else
    rsync -vr --progress dist/* michaelcouch@michaelcouch.io:/home/michaelcouch/michaelcouch.io/$branch/.
fi

