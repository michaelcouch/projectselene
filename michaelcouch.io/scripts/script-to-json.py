#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import json
import io
import time as tm
#with io.open(fn, 'rt', newline='') as f:
#    lines = f.readlines()

OFFSETS = [
  ["",""],
#  [,],
]

def parse_script_to_json(text_file, timing_file):
    print('getting script')

    offsets = OFFSETS.__iter__()
    item, offset = next(offsets)

    with io.open(text_file, 'rb') as f:
        fulltext = f.read()
    fulltext = fulltext[fulltext.find(str.encode('\n'))+1:]
    print('getting timings')
    with open(timing_file, 'r') as f:
        times = [[time, int(n)] for (time, n) in map(lambda a: a.split(), f.readlines())]
    data = []
    n_events = len(times)
    i = 0
    for time, n in times:
        i += 1
        #print(i, ' of ' , n_events)
        if i == item:
            n = n + offset
            item, offset = next(offsets)
        text, fulltext = bytes.decode(fulltext[:n]), fulltext[n:]
        #tm.sleep(float(time))
        #print(i)
        print(text)
        #print(str.encode(text))
        #input ()
        data.append({'event': i, 'text': text, 'timeDelay': time, 'nchar': n})
    #print(time)

    return data #[{'text': 'tst', 'time_delay': 0.3 },
           # {'text': 't2t', 'timeDelay': 0.2}]


if __name__ == "__main__":
    text_file, timing_file = sys.argv[1:3]
    data = parse_script_to_json(text_file, timing_file)
    if len(sys.argv) == 4:
        out_file = sys.argv[3]
        with open(out_file,'w') as f:
            json.dump(data, f, indent=2)
    else:
        print(json.dumps(data, indent=2))
