#!/bin/bash

wget -O data/sa4_2016.zip "https://www.abs.gov.au/AUSSTATS/subscriber.nsf/log?openagent&1270055001_sa4_2016_aust_shape.zip&1270.0.55.001&Data%20Cubes&C65BC89E549D1CA3CA257FED0013E074&0&July%202016&12.07.2016&Latest"

unzip -o data/sa4_2016.zip -d data
ogr2ogr -f GeoJSON -sql "SELECT SA4_CODE16, SA4_NAME16 from SA4_2016_AUST" data/sa4_2016.json data/SA4_2016_AUST.shp -simplify 0.01
