const math = require('../../node_modules/mathjs')

function time_step (state, delta_t) {
  state.delta_t = delta_t;
  state.pos = math.evaluate('pos + vel * delta_t', state)
  state.delta_v = net_force(state)
  state.delta_v = math.dotDivide(state.delta_v,state.mass1)
  state.vel = math.evaluate('vel + delta_v * delta_t', state)
}

function net_force (state) {
  let net_forces = [];
  for (var i = 0; i < state.pos.size()[0]; i++){
    let force = get_electric_force(i, state)
    net_forces.push(force)
  }
  return net_forces
}

function get_electric_force(i, state) {
   let element = state.pos.subset(math.index(i,[0,1]))
   let charg  = state.charge.subset(math.index(i))
   var force = math.multiply(-1, math.subset(state.pos, math.index(i,[0,1])))
   var drag = math.multiply(-0.001* math.norm(math.squeeze(math.subset(state.vel, math.index(i,[0,1])))), math.subset(state.vel, math.index(i,[0,1])))
   force = math.add(force, drag)
  for (var n = 0; n < state.pos.size()[0]; n++ ) {
    if (n != i) {
      var atom = math.subset(state.pos, math.index(n,[0,1]))
      var atomcharge  = state.charge.subset(math.index(n))
      var displacement = math.subtract(element, atom);
      var distance = math.norm(math.squeeze(displacement))
//      console.log( math.max(1,math.norm(0.1 * charg * atomcharge / distance ** 4)) + charg * atomcharge / distance ** 3)
      force = math.add(force,
                math.multiply(displacement,
                   math.norm(0.0125 * charg * atomcharge / (distance * (distance + 0.5)**3))
                   + 25 * charg * atomcharge / (distance * (distance + 0.5)**2)));
    }
  }
  return math.squeeze(force)
}

function generate_state(n) {
  let pos = [];
  let vel = [];
  let charge = [];
  let mass = [];
  let mass1 = [];
  for (var i = 0; i < n; i++) {
    pos.push([2*math.random() - 1, 2*math.random() - 1]);
    vel.push([0.05*math.random() - 0.025, 0.05*math.random() - 0.025]);
    let m = 0
    if (i != n-1) {
      do {
        m = math.floor(math.random()*5) - 2;
      } while (m == 0)
    } else {
      m = -math.sum(charge) 
    }
    charge.push(m);
    if (m < 0) {
      mass.push([0.5, 0.5]);
      mass1.push([0.5**3, 0.5**3]);
    } else {
      mass.push([1, 1]);
      mass1.push([1**3, 1**3]);
    }
  }
  let state = {
    pos: math.matrix(pos),
    vel: math.matrix(vel),
    mass: math.matrix(mass),
    mass1: math.matrix(mass1),//the true mass, radius**3
    charge: math.matrix(charge),
    delta_t: null,
    delta_v: null
  }
  return state 
}

export default {generate_state, time_step}
