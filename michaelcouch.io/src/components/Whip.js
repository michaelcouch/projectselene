const math = require('../../node_modules/mathjs')

let whip = {
  pos: math.matrix([[0, 0], [0,  1], [1, 0], [1, 1], [2, 0], [2, 1], [3, 0], [3, 1], [4, 0], [4, 1], [5, 0], [5, 1], [6, 0], [6, 1], [7, 0], [7, 1], [8, 0], [8, 1], [9, 0], [9, 1], [10, 0], [10, 1], [11, 0], [11, 1],]),
  vel: math.matrix([[-3,-1], [-2,-2], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0],  [0, 0],  [0, 0],  [0, 0],]),
  mass: math.matrix([1/2,             1/2.5,        1/3,            1/3.5,          1/4,            1/4.5 ,         1/5,            1/5.5,          1/6,            1/6.5,          1/7,              1/7.5,     ]),
  delta_t: null,
  net_force: null
}

function time_step (whip, delta_t) {
  whip.delta_t = delta_t;
  whip.pos = math.evaluate('pos + vel * delta_t', whip)
  whip.net_force = net_force(whip)
  whip.vel = math.evaluate('(0.9^delta_t) * vel + net_force * delta_t', whip)
}

function net_force (whip) {
  let net_forces = [];
  for (var i = 0; i < whip.pos.size()[0]; i++){
    let element = whip.pos.subset(math.index(i,[0,1]))
    let vel     = whip.vel.subset(math.index(i,[0,1]))
    let neighbours = get_neighbours(whip, i)
    let force = math.divide(get_element_spring_force(element, neighbours),
                            whip.mass.subset(math.index(math.floor(i/2))))
    //force = math.subtract(force, math.multiply(vel, 0.1)) 
    net_forces.push(force)
  }
  return net_forces
}

function get_neighbours(whip, i) {
  const spring_length = 1;
  var neighbours = [];
  neighbours.push([whip.pos.subset(math.index(i+(-1)**i,[0, 1])),spring_length])
  if (i > 1) {
    neighbours.push([whip.pos.subset(math.index(i-2,[0, 1])),spring_length])
    neighbours.push([whip.pos.subset(math.index(i-2+(-1)**i,[0, 1])),spring_length*math.sqrt(2)])
  }
  if (i < whip.pos.size()[0]-2) {
    neighbours.push([whip.pos.subset(math.index(i+2,[0, 1])),spring_length])
    neighbours.push([whip.pos.subset(math.index(i+2+(-1)**i,[0, 1])),spring_length*math.sqrt(2)])
  }
  return neighbours
}

function get_element_spring_force(element, neighbours) {
  var force = math.matrix([[0,0]])
  for (var n = 0; n < neighbours.length; n++ ) {
    var neighbour = neighbours[n]
    var displacement = math.subtract(neighbour[0],element);
    var distsqr = math.multiply(displacement,math.transpose(displacement) )
    var distance = math.sqrt(distsqr.subset(math.index(0,0)));
    var natural_length = neighbour[1];
    force = math.add(force, math.multiply(displacement,  (distance - natural_length) /distance));
  }
  return math.squeeze(force)
}

export default {whip, time_step}
