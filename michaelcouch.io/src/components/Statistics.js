const math = require('../../node_modules/mathjs')


function randomNormal() {
      //
      var u = 0, v = 0;
      while(u === 0) u = math.random(); //Converting [0,1) to (0,1)
      while(v === 0) v = math.random();
      return math.sqrt( -2.0 * math.log( u ) ) * math.cos( 2.0 * Math.PI * v );
}

function generateLine(n, x0, x1, c, m, xstddev, ystddev) {
  let points = [];
  let params = [c, m];
  let degree = 1;
  for (var j = 0; j < n; j++) {
    let x = x0 + (j + randomNormal()*xstddev / 4) * (x1 - x0) / n //95% prob between x0, x1
    let y = randomNormal() * ystddev
    for (let i=0; i < degree + 1; i++) {
      y += poly(x,i) * params[i]
    }
    points.push([x, y]);
    }
  return points
}
function generateRandomCurvilinearPoints(n, x0, x1, degree, stddev) {
  let points = [];
  let params = []
  for (let i=0; i < degree + 1; i++) {
    let c = randomNormal() / (i+1)
    params.push(c)
  }
  for (var j = 0; j < n; j++) {
    let x = x0 + ((j + 0*randomNormal()/4) / n) * (x1 - x0) //95% prob between x0, x1
    let y = randomNormal() * stddev
    for (let i=0; i < degree + 1; i++) {
      y += poly(x,i) * params[i]
    }
    points.push([x, y]);
    }
  let state = {
    points: math.matrix(points),
    params: [params]
  }
  return state
}

function generatePoints(n) {
  let points = [];
  let bestFit = [];
  for (var i = 0; i < n; i++) {
    points.push([2*math.random() - 1, 2*math.random() - 1]);
    bestFit.push([2*math.random() - 1, 2*math.random() - 1]);
    }
  let state = {
    points: math.matrix(points),
  }
  return state
}

function merge(state1, state2) {
  return {
    points: math.concat(state1.points,state2.points,0),
    params: state1.params.concat(state2.params)
  }
}

function computeBestFit({points}, n) {
  points = math.squeeze(math.matrix(points))
  let size = points.size()[0]
  let xi = math.subset(points, math.index(math.range(0,size), [0]));
  let yi = math.subset(points, math.index(math.range(0,size), [1]));
  let XX = [];
  for (let x of xi._data) {
    let el = [];
    for (let i=0; i < n + 1; i++) {
      el.push(poly(x[0],i))
    }
    XX.push(el)
  }

  let X = math.matrix(XX)
  let XT = math.transpose(X)
  let params = math.lusolve(
    math.multiply(XT, X),
    math.multiply(XT, yi)
  )
  return {
    params,
    bestFitPoints: math.concat(xi,math.multiply(X, params))
  }
}

function poly(x,n) {
  return  math.pow(x,n)
}


//x=generateRandomCurvilinearPoints(5, 0, 2, 1, 0.000000)
//y=generateRandomCurvilinearPoints(5, 0, 2, 1, 0.000000)
//console.log(merge(x,y).points)

  //state = generatePoints(5)
//console.log(computeBestFit(state,1))
export default {
  generatePoints,
  generateLine,
  randomNormal,
  merge,
  generateRandomCurvilinearPoints,
  computeBestFit
}
