import Vue from 'vue'
import Router from 'vue-router'
const DataMapping  = () => import('@/components/DataMapping.vue')
const MachineStats = () => import('@/components/MachineStats.vue')
const MathModel    = () => import('@/components/MathModel.vue')
const FrontBack    = () => import('@/components/FrontBack.vue')

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/stat',
      name: 'stat',
      component: MachineStats,
      props: {msg:''}
    },
    {
      path: '/math',
      name: 'Math',
      component: MathModel,
      props: {msg: ''}
    },
    {
      path: '/tech',
      name: 'tech',
      component: FrontBack,
      props: {msg: ""}
    },
    {
      path: '/data',
      name: 'data',
      component: DataMapping,
      props: {msg: ""}
    },
  ]
})
